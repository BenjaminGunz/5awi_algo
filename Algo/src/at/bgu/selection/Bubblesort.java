package at.bgu.selection;

public class Bubblesort {

	static void bubbleSort(int[] ArrayOfNumbers) {

		int n = ArrayOfNumbers.length;
		int temp = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {
				if (ArrayOfNumbers[j - 1] > ArrayOfNumbers[j]) {
					// swap elements
					temp = ArrayOfNumbers[j - 1];
					ArrayOfNumbers[j - 1] = ArrayOfNumbers[j];
					ArrayOfNumbers[j] = temp;
				}

			}

		}

	}

}

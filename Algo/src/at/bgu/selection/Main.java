package at.bgu.selection;

public class Main {

	public static void main(String[] args) {
		int ArrayOfNumbers[] = { 2, 5, 99, 34, 4, 8, 320 };
		
		System.out.println("Array before Bubble Sort");
		for (int i = 0; i < ArrayOfNumbers.length; i++) {
			System.out.println(ArrayOfNumbers[i] + "");
		}
		System.out.println();
		
		
		Bubblesort.bubbleSort(ArrayOfNumbers);
		
		System.out.println("Array after Bubble Sort");
		for (int i = 0; i < ArrayOfNumbers.length; i++) {
			System.out.println(ArrayOfNumbers[i] + "");
		}
	}

}
